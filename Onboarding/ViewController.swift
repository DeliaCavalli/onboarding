//
//  ViewController.swift
//  Onboarding
//
//  Created by Delia Cavalli on 05/06/2020.
//  Copyright © 2020 Delia Cavalli. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var startButton: UIButton!
    
    var scrollWidth: CGFloat = 0.0
    var scrollHeight: CGFloat = 0.0
    
    //data for the slides
    var headings = ["Hello Stranger!","Everyone can lose their compass","Defeat monsters!"]
    var subheadings = ["I'm (name) the Samurai and I will accompany you on this adventure!","I'm here to help you set good habits and improve your well-being.","Taking on the monsters challenges will damage them: be steady and defeat them!"]
    var images = ["image1","image2","image3"]
    
    //get dynamic width and height of scrollview and save it
    override func viewDidLayoutSubviews() {
        scrollWidth = scrollView.frame.size.width
        scrollHeight = scrollView.frame.size.height
    }
    
    //to call viewDidLayoutSubviews() and get dynamic width and height of scrollview
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        
        self.scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        //crete the slides and add them
        var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        
        for index in 0..<headings.count {
            frame.origin.x = scrollWidth * CGFloat(index)
            frame.size = CGSize(width: scrollWidth, height: scrollHeight)
            
            let slide = UIView(frame: frame)
            
            //subviews
            let imageView = UIImageView.init(image: UIImage.init(named: images[index]))
            imageView.frame = CGRect(x:0,y:0,width:300,height:300)
            imageView.contentMode = .scaleAspectFit
            imageView.center = CGPoint(x:scrollWidth/2,y: scrollHeight/2 - 50)
            
            let text1 = UILabel.init(frame: CGRect(x:32,y:imageView.frame.maxY+30,width:scrollWidth-64,height:30))
            text1.textAlignment = .center
            text1.font = UIFont.boldSystemFont(ofSize: 20.0)
            text1.text = headings[index]
            
            let text2 = UILabel.init(frame: CGRect(x:32,y:text1.frame.maxY+10,width:scrollWidth-64,height:50))
            text2.textAlignment = .center
            text2.numberOfLines = 3
            text2.font = UIFont.systemFont(ofSize: 18.0)
            text2.text = subheadings[index]
            
            slide.addSubview(imageView)
            slide.addSubview(text1)
            slide.addSubview(text2)
            scrollView.addSubview(slide)
            
        }
        //set width of scrollview to accomodate all the slides
        scrollView.contentSize = CGSize(width: scrollWidth * CGFloat(headings.count), height: scrollHeight)
        
        //disable vertical scroll/bounce
        self.scrollView.contentSize.height = 1.0
        
        //initial state
        pageControl.numberOfPages = headings.count
        pageControl.currentPage = 0
        
    }
    
    //indicator
    
    @IBAction func pageChanged(_ sender: Any) {
        scrollView!.scrollRectToVisible(CGRect(x: scrollWidth * CGFloat ((pageControl?.currentPage)!), y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setIndiactorForCurrentPage()
    }
    
    func setIndiactorForCurrentPage()  {
        let page = (scrollView?.contentOffset.x)!/scrollWidth
        pageControl?.currentPage = Int(page)
    }
    
}





